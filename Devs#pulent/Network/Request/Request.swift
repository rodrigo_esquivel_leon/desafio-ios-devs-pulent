//
//  Request.swift
//  Devs#pulent
//
//  Created by Rodrigo on 10/19/19.
//  Copyright © 2019 Dorito Studio. All rights reserved.
//

import Foundation

struct Request: RequestProtocol {
    
    typealias ResponseType = SearchResponse
    var completion: ((Result<SearchResponse?>) -> Void)?
    var method: HTTPMethod = .get
    var url: URL?
    var encodableBody: Encodable?
    var simulatedResponseJSONFile: String?
    var verbose: Bool?
    var contentType: ContentType = .json
    var processHeader: Bool?
    var shouldRetry: Bool = true
    
    init(search: String?) {
        url = API.searchSongsBy(search: search).url
    }
    
}
