//
//  RequestWorker.swift
//  Devs#pulent
//
//  Created by Rodrigo on 10/19/19.
//  Copyright © 2019 Dorito Studio. All rights reserved.
//

import Foundation

class RequestWorker {
    
    private let defaultRetryTime: TimeInterval = 2.0
    
    fileprivate func sessionConfiguration() -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 30
        configuration.httpAdditionalHeaders = ["":""]
        return configuration
    }
    
    var isConnected: Bool {
        // TODO: Connection state handler.
        return true
    }
    
    func send<T: RequestProtocol>(request: T, after: TimeInterval) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + after) {
            self.send(request: request)
        }
    }
    
    func send<T: RequestProtocol>(request: T) {
        
        guard isConnected else {
            send(request: request, after: defaultRetryTime)
            return
        }
        
        guard var url = request.url else {
            assertionFailure("Error creating URL for endpoint: \(String(describing: request))")
            request.completion?(Result.failure(ResultError.internalError(message: "Error creating URL for endpoint: \(String(describing: request))")))
            return
        }
        
        if let queryDictionary = request.queryString {
            let urlParams = queryDictionary.compactMap({ (key, value) -> String in
                return "\(key)=\(value)"
            }).joined(separator: "&")
            
            let queryString = url.absoluteString + "?" + urlParams
            if let queryURL = URL(string: queryString) {
                url = queryURL
            }
        }
        
        var httpRequest = URLRequest(url: url)
        httpRequest.httpMethod = request.method.rawValue
        
        if request.method == .post || request.method == .put {
            httpRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            httpRequest.httpBody = request.body
        }
        
        let session = URLSession(configuration: sessionConfiguration())
        
        let task = session.dataTask(with: httpRequest) { (serverData, responseData, error) in
            DispatchQueue.main.async {
                
                if let error = error {
                    let httpError = ResultError(error: error)
                    // Check if it's a generic connection error to attempt retry
                    if httpError.isConnectionError && request.shouldRetry {
                        self.send(request: request, after: self.defaultRetryTime)
                    } else {
                        request.completion?(Result.failure(httpError))
                    }
                    return
                }
                
                let httpCode: HTTPCode = HTTPCode(intValue: (responseData as? HTTPURLResponse)?.statusCode ?? 0)
                
                if !httpCode.isSuccess {
                    var dataDict: [String : Any] = ["Request": request.debugDescription]
                    dataDict.updateValue(String(data: (serverData ?? Data()), encoding: .utf8) ?? "", forKey: "Response")
                    
                    guard let data = serverData else {
                        request.completion?(Result.failure(ResultError.networkError(code: httpCode)))
                        return
                    }
                    
                    do {
                        
                        let error = try JSONDecoder().decode(RequestErrorResponse.self, from: data)
                        print("Received Server error. Reason: \((error.reason ?? "Unknown")). Info: \(error.errors)")
                        request.completion?(Result.failure(ResultError.serverError(code: httpCode, underlying: error)))
                        
                    } catch {
                        print("Unknown error: \(error)")
                        if let errorDescription = String(data: data, encoding: .utf8) {
                            print(errorDescription)
                        }
                        
                        request.completion?(Result.failure(ResultError.unknownError(code: httpCode, underlying: error)))
                    }
                    
                    return
                }
                
                var informationData = serverData
                if let processHeader = request.processHeader, processHeader == true, let responseData = responseData as? HTTPURLResponse {
                    informationData = try? JSONSerialization.data(withJSONObject: responseData.allHeaderFields, options: .prettyPrinted)
                }
                
                guard request.method != .delete, let data = informationData else {
                    request.completion?(Result.success(nil))
                    return
                }
                do {
                    let processedData = try request.processResponseData(data: data)
                    
                    request.completion?(Result.success(processedData))
                    
                } catch {
                    request.completion?(Result.failure(ResultError.parsingError(message: error.localizedDescription)))
                }
                
            }
            
        }
        
        task.resume()
    }
}
