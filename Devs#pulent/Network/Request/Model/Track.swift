//
//  Track.swift
//  Devs#pulent
//
//  Created by Rodrigo on 10/19/19.
//  Copyright © 2019 Dorito Studio. All rights reserved.
//

import Foundation

public struct SearchResponse: Decodable {
    let resultCount: Int
    let results: [TrackResponse]
}

public struct TrackResponse: Decodable {
    let artistName: String
    let trackId: Int
    let trackName: String
    let trackViewUrl: String
    let previewUrl: String?
    let artworkUrl100: String
    let releaseDate: String
    let primaryGenreName: String
    let trackPrice: Float?
    let currency: String
    let trackTimeMillis: Int?
    let collectionName: String?
}
