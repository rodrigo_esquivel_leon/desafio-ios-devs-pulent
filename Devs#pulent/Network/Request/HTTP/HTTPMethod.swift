//
//  HTTPMethod.swift
//  Devs#pulent
//
//  Created by Rodrigo on 10/19/19.
//  Copyright © 2019 Dorito Studio. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get
    case head
    case post
    case put
    case patch
    case delete = "DELETE"
}
