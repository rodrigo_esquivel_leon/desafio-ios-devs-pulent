//
//  API.swift
//  Devs#pulent
//
//  Created by Rodrigo on 10/19/19.
//  Copyright © 2019 Dorito Studio. All rights reserved.
//

import Foundation

enum API: APIProtocol {
    
    var rawValue: String {
        switch self {
        case .searchSongsBy(let search):
            var endpoint = "?\(APIUrl.InputKeys.mediaType)=\(APIUrl.InputValues.mediaType)&\(APIUrl.InputKeys.limit)=\(APIUrl.InputValues.limit)"
            
            if let search = search, let searchWithUrlFormat = search.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                endpoint = "\(endpoint)&\(APIUrl.InputKeys.term)=\(searchWithUrlFormat)"
            }
            
            return endpoint
        }
    }
    
    case searchSongsBy(search: String?)
}
