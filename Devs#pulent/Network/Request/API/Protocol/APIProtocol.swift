//
//  APIProtocol.swift
//  Devs#pulent
//
//  Created by Rodrigo on 10/19/19.
//  Copyright © 2019 Dorito Studio. All rights reserved.
//

import Foundation

protocol APIProtocol: RawRepresentable where RawValue == String {
    var url: URL? { get }
}

struct APIUrl {
    static let baseURL: String = "https://itunes.apple.com/search"
    
    struct InputKeys {
        static let term: String = "term"
        static let mediaType: String = "mediaType"
        static let limit: String = "limit"
    }
    
    struct InputValues {
        static let mediaType: String = "music"
        static let limit: Int = 20
    }
}

extension APIProtocol {
    
    init?(rawValue: String) {
        assertionFailure("init(rawValue:) not implemented")
        return nil
    }
    
    var url: URL? {
        let urlComponents = URLComponents(string: APIUrl.baseURL + self.rawValue)
        return urlComponents?.url
    }
    
}
