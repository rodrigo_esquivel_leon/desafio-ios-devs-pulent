//
//  SearchDetailRouter.swift
//  Devs#pulent
//
//  Created by Rodrigo on 10/20/19.
//  Copyright (c) 2019 Dorito Studio. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol SearchDetailRoutingLogic
{
    func routeToSearchDetail(track: TrackResponse)
}

protocol SearchDetailDataPassing
{
    var dataStore: SearchDetailDataStore? { get }
}

class SearchDetailRouter: NSObject, SearchDetailRoutingLogic, SearchDetailDataPassing
{
    var navigationController: UINavigationController?
    var viewController: SearchDetailViewController?
    var dataStore: SearchDetailDataStore?
    
    static let shared = SearchDetailRouter()
    
    private override init() { }
    
    // MARK: Routing
    func routeToSearchDetail(track: TrackResponse)
    {
        viewController = SearchDetailViewController.init(nibName: "SearchDetailView", bundle: nil)
        viewController?.track = track
        navigationController?.pushViewController(viewController!, animated: true)
    }
    
}
