# Desafío iOS devs #pulent

Search songs using iTunes API developed in Swift

## Installation

I used Alamofire and AlamofireImage to render the album cover in the search list as well as the search detail. So, it is required to run the ```pod install``` in the project directory then open the ```.xcworkspace```

If you do not have pods installed in your machine, run ```sudo gem install cocoapods```.

## License
[MIT](https://choosealicense.com/licenses/mit/)
